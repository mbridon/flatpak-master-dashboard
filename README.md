# flatpak-master-dashboard

This is just a quick and dirty script to make a dashboard of the latest builds
from [gnome-sdk-images](https://gitlab.gnome.org/GNOME/gnome-sdk-images) and
[gnome-apps-nightly](https://gitlab.gnome.org/GNOME/gnome-apps-nightly).

It helps finding the latest build logs, to make it easier to fix build issues
and ensure everything keeps building.

**This is not meant to be a permanent solution.**

If you're considering contributing to this repository, think about whether you
could instead contribute to
[the future we want](https://gitlab.gnome.org/GNOME/gnome-build-meta).
