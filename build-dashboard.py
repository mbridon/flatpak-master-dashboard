#!/usr/bin/env python3


# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


from datetime import datetime, timezone
import json
import re
import sys

from bs4 import BeautifulSoup
from jinja2 import Template
import requests


LOG_URLS = [
    {
        'url': 'http://sdkbuilder.gnome.org/logs',
        'arches': ('x86_64', 'i386'),
    },
    {
        'url': 'https://gnome1.codethink.co.uk/logs',
        'arches': ('aarch64', ),
    }
]
URL_PATTERN = re.compile(r'(http\S+)')


class Pipeline:
    APP_FILE_NAME_PATTERN = re.compile(r'gnome-apps-nightly-([a-zA-Z0-9.]+)-([a-zA-Z0-9.-]+)-([a-zA-Z0-9_]+)$')
    SDK_FILE_NAME_PATTERN = re.compile(r'^build-gnome-sdk-images-([a-zA-Z0-9.-]+)-([a-zA-Z0-9_]+).txt$')

    def __init__(self, root_url):
        self.root_url = root_url
        self.url = f'{root_url}/build.txt'

    def _get_log(self):
        response = requests.get(self.url)

        return response.text

    def _get_app_build_from_log_line(self, line):
        _, status, file_name = line.rsplit(' ', 2)

        # status is either 'succeeded:' or 'failed:'
        status = {'succeeded:': 'success', 'failed:': 'failure'}[status]

        match = self.APP_FILE_NAME_PATTERN.match(file_name)

        if match is None:
            raise ValueError(f'Invalid line for an app build: {line}')

        name = match.group(2)
        branch = match.group(1)
        arch = match.group(3)

        return Build(name, arch, branch, status, f'{self.root_url}/build-{file_name}.txt')

    def _get_sdk_build_from_log_line(self, line):
        _, status, file_name = line.rsplit(' ', 2)

        # status is either 'success:' or 'failed:'
        status = {'success:': 'success', 'failed:': 'failure'}[status]

        match = self.SDK_FILE_NAME_PATTERN.match(file_name)

        if match is None:
            raise ValueError(f'Invalid line for an Sdk build: {line}')

        branch = match.group(1)
        arch = match.group(2)

        return Build('org.gnome.Sdk', arch, branch, status, f'{self.root_url}/{file_name}')

    def get_builds(self):
        for line in self._get_log().split('\n'):
            if '] SDK build ' in line:
                yield self._get_sdk_build_from_log_line(line)

            elif line.startswith('Build succeeded:') or line.startswith('Build failed:'):
                yield self._get_app_build_from_log_line(line)


class Build:
    def __init__(self, name, arch, branch, status, log_url):
        self.name = name
        self.arch = arch
        self.branch = branch
        self.status = status
        self.log_url = log_url

        self._notes = ''

    @property
    def datetime(self):
        pipeline_name = self.log_url.rsplit('/', 2)[1]
        dt = pipeline_name[6:]
        dt = datetime.strptime(dt, '%Y-%m-%d-%H%M%S')

        return dt.strftime('%Y-%m-%d %H:%M')

    @property
    def ref(self):
        return f'{self.name}/{self.arch}/{self.branch}'

    @property
    def notes(self):
        return self._notes

    @notes.setter
    def notes(self, notes):
        if self.status == 'failure':
            self._notes = URL_PATTERN.sub(r'<a href="\1">\1</a>', notes)


class WipNotes:
    def __init__(self, file_path):
        with open(wip_file) as f:
            self._wip_notes = json.load(f)

    def get(self, build):
        notes = self._wip_notes.get(build.ref, '')

        if notes:
            return notes

        return self._wip_notes.get(f'{build.name}/*/{build.branch}', '')


def get_pipelines(root_url, max=None):
    response = requests.get(root_url)
    index = BeautifulSoup(response.text, 'html.parser')

    pipelines = [a.text for a in index.find_all('a') if a.text.startswith('build-')]
    pipelines = sorted(pipelines, reverse=True)

    if max is not None:
        pipelines = pipelines[:max]

    for pipeline in pipelines:
        if pipeline.endswith('/'):
            pipeline = pipeline[:-1]

        yield Pipeline(f'{root_url}/{pipeline}')


def now():
    return datetime.now(tz=timezone.utc).strftime('%Y-%m-%d %H:%M') + " UTC"


def build_sort_key(build):
    return build.ref.lower()


template_file = sys.argv[1]
wip_file = sys.argv[2]
output_file = sys.argv[3]

builds = {}
wip_notes = WipNotes(wip_file)

for logdata in LOG_URLS:
    log_url = logdata['url']
    arches = logdata['arches']

    for last_pipeline in get_pipelines(log_url, max=20):
        for build in last_pipeline.get_builds():
            if build.ref not in builds:
                build.notes = wip_notes.get(build)
                builds[build.ref] = build

builds = sorted(builds.values(), key=build_sort_key)
num_success = len([b for b in builds if b.status == 'success'])
num_failed = len([b for b in builds if b.status == 'failure'])
num_total = len(builds)
now = now()

with open(template_file, 'r') as f:
    template = Template(f.read())

report = template.render(
        builds=builds, num_success=num_success, num_failed=num_failed,
        num_total=num_total, last_updated=now)

with open(output_file, 'w') as f:
    f.write(report)
